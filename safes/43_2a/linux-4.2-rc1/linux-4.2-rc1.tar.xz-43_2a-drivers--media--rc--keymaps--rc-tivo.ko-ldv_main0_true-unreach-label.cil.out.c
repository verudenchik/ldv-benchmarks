/* Generated by CIL v. 1.5.1 */
/* print_CIL_Input is false */

#line 16 "include/asm-generic/int-ll64.h"
typedef unsigned char u8;
#line 22 "include/asm-generic/int-ll64.h"
typedef unsigned int u32;
#line 157 "include/linux/types.h"
typedef unsigned int gfp_t;
#line 177 "include/linux/types.h"
struct __anonstruct_atomic_t_6 {
   int counter ;
};
#line 177 "include/linux/types.h"
typedef struct __anonstruct_atomic_t_6 atomic_t;
#line 183 "include/linux/types.h"
struct list_head {
   struct list_head *next ;
   struct list_head *prev ;
};
#line 20 "./arch/x86/include/asm/spinlock_types.h"
struct qspinlock {
   atomic_t val ;
};
#line 33 "include/asm-generic/qspinlock_types.h"
typedef struct qspinlock arch_spinlock_t;
#line 33 "include/linux/bottom_half.h"
struct lockdep_map;
#line 55 "include/linux/debug_locks.h"
struct stack_trace {
   unsigned int nr_entries ;
   unsigned int max_entries ;
   unsigned long *entries ;
   int skip ;
};
#line 28 "include/linux/stacktrace.h"
struct lockdep_subclass_key {
   char __one_byte ;
};
#line 53 "include/linux/lockdep.h"
struct lock_class_key {
   struct lockdep_subclass_key subkeys[8U] ;
};
#line 59 "include/linux/lockdep.h"
struct lock_class {
   struct list_head hash_entry ;
   struct list_head lock_entry ;
   struct lockdep_subclass_key *key ;
   unsigned int subclass ;
   unsigned int dep_gen_id ;
   unsigned long usage_mask ;
   struct stack_trace usage_traces[13U] ;
   struct list_head locks_after ;
   struct list_head locks_before ;
   unsigned int version ;
   unsigned long ops ;
   char const   *name ;
   int name_version ;
   unsigned long contention_point[4U] ;
   unsigned long contending_point[4U] ;
};
#line 144 "include/linux/lockdep.h"
struct lockdep_map {
   struct lock_class_key *key ;
   struct lock_class *class_cache[2U] ;
   char const   *name ;
   int cpu ;
   unsigned long ip ;
};
#line 546 "include/linux/lockdep.h"
struct raw_spinlock {
   arch_spinlock_t raw_lock ;
   unsigned int magic ;
   unsigned int owner_cpu ;
   void *owner ;
   struct lockdep_map dep_map ;
};
#line 33 "include/linux/spinlock_types.h"
struct __anonstruct____missing_field_name_35 {
   u8 __padding[24U] ;
   struct lockdep_map dep_map ;
};
#line 33 "include/linux/spinlock_types.h"
union __anonunion____missing_field_name_34 {
   struct raw_spinlock rlock ;
   struct __anonstruct____missing_field_name_35 __annonCompField17 ;
};
#line 33 "include/linux/spinlock_types.h"
struct spinlock {
   union __anonunion____missing_field_name_34 __annonCompField18 ;
};
#line 76 "include/linux/spinlock_types.h"
typedef struct spinlock spinlock_t;
#line 32 "include/linux/mm_types.h"
struct kmem_cache;
#line 532 "include/linux/input.h"
enum rc_type {
    RC_TYPE_UNKNOWN = 0,
    RC_TYPE_OTHER = 1,
    RC_TYPE_LIRC = 2,
    RC_TYPE_RC5 = 3,
    RC_TYPE_RC5X = 4,
    RC_TYPE_RC5_SZ = 5,
    RC_TYPE_JVC = 6,
    RC_TYPE_SONY12 = 7,
    RC_TYPE_SONY15 = 8,
    RC_TYPE_SONY20 = 9,
    RC_TYPE_NEC = 10,
    RC_TYPE_SANYO = 11,
    RC_TYPE_MCE_KBD = 12,
    RC_TYPE_RC6_0 = 13,
    RC_TYPE_RC6_6A_20 = 14,
    RC_TYPE_RC6_6A_24 = 15,
    RC_TYPE_RC6_6A_32 = 16,
    RC_TYPE_RC6_MCE = 17,
    RC_TYPE_SHARP = 18,
    RC_TYPE_XMP = 19
} ;
#line 555 "include/linux/input.h"
struct rc_map_table {
   u32 scancode ;
   u32 keycode ;
};
#line 83 "include/media/rc-map.h"
struct rc_map {
   struct rc_map_table *scan ;
   unsigned int size ;
   unsigned int len ;
   unsigned int alloc ;
   enum rc_type rc_type ;
   char const   *name ;
   spinlock_t lock ;
};
#line 93 "include/media/rc-map.h"
struct rc_map_list {
   struct list_head list ;
   struct rc_map map ;
};
#line 29 "include/linux/types.h"
typedef _Bool bool;
#line 361 "./arch/x86/include/asm/pgtable_types.h"
struct page;
#line 26 "/home/cluser/ldv/inst/kernel-rules/verifier/rcv.h"
extern void *ldv_undef_ptr(void) ;
#line 293 "include/linux/slab.h"
void *ldv_kmem_cache_alloc_20(struct kmem_cache *ldv_func_arg1 , gfp_t flags ) ;
#line 18 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-tivo.o.c.prepared"
void ldv_check_alloc_flags(gfp_t flags ) ;
#line 101 "include/media/rc-map.h"
extern int rc_map_register(struct rc_map_list * ) ;
#line 102
extern void rc_map_unregister(struct rc_map_list * ) ;
#line 23 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-tivo.c"
static struct rc_map_table tivo[45U]  = 
#line 23 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-tivo.c"
  {      {2701955087U, 226U}, 
        {2701920263U, 356U}, 
        {2701953031U, 377U}, 
        {2701929475U, 241U}, 
        {2701969415U, 358U}, 
        {2701982213U, 154U}, 
        {8728671U, 154U}, 
        {2701945859U, 365U}, 
        {2701928455U, 103U}, 
        {2701944839U, 108U}, 
        {2701977607U, 105U}, 
        {2701961223U, 106U}, 
        {2701924359U, 178U}, 
        {2701957127U, 353U}, 
        {2701940743U, 177U}, 
        {2701932551U, 115U}, 
        {2701965319U, 114U}, 
        {2701973511U, 113U}, 
        {2701919243U, 167U}, 
        {2701948935U, 402U}, 
        {2701981703U, 403U}, 
        {8728607U, 403U}, 
        {2701952011U, 207U}, 
        {2701968395U, 119U}, 
        {2701960203U, 409U}, 
        {2701935627U, 168U}, 
        {2701927435U, 208U}, 
        {2701943819U, 412U}, 
        {2701976587U, 407U}, 
        {2701926925U, 372U}, 
        {2701922829U, 128U}, 
        {2701939213U, 389U}, 
        {2701923339U, 513U}, 
        {2701956107U, 514U}, 
        {2701939723U, 515U}, 
        {2701972491U, 516U}, 
        {2701931531U, 517U}, 
        {2701964299U, 518U}, 
        {2701947915U, 519U}, 
        {2701980683U, 520U}, 
        {8728623U, 520U}, 
        {2701921283U, 521U}, 
        {2701954051U, 512U}, 
        {2701970435U, 28U}, 
        {2701937667U, 355U}};
#line 77 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-tivo.c"
static struct rc_map_list tivo_map  =    {{0, 0}, {(struct rc_map_table *)(& tivo), 45U, 0U, 0U, 10, "rc-tivo", {{{{{0}},
                                                                             0U, 0U,
                                                                             0, {0,
                                                                                 {0,
                                                                                  0},
                                                                                 0,
                                                                                 0,
                                                                                 0UL}}}}}};
#line 86 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-tivo.c"
static int init_rc_map_tivo(void) 
{ 
  int tmp ;

  {
#line 88
  tmp = rc_map_register(& tivo_map);
#line 88
  return (tmp);
}
}
#line 91 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-tivo.c"
static void exit_rc_map_tivo(void) 
{ 


  {
#line 93
  rc_map_unregister(& tivo_map);
#line 94
  return;
}
}
#line 118
extern void ldv_check_final_state(void) ;
#line 127
extern void ldv_initialize(void) ;
#line 130
extern void ldv_handler_precall(void) ;
#line 133
extern int nondet_int(void) ;
#line 136 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-tivo.c"
int LDV_IN_INTERRUPT  ;
#line 139 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-tivo.c"
void main(void) 
{ 
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;

  {
#line 151
  LDV_IN_INTERRUPT = 1;
#line 160
  ldv_initialize();
#line 166
  ldv_handler_precall();
#line 167
  tmp = init_rc_map_tivo();
#line 167
  if (tmp != 0) {
#line 168
    goto ldv_final;
  } else {

  }
#line 170
  goto ldv_25641;
  ldv_25640: 
#line 173
  tmp___0 = nondet_int();
#line 173
  switch (tmp___0) {
  default: ;
#line 175
  goto ldv_25639;
  }
  ldv_25639: ;
  ldv_25641: 
#line 170
  tmp___1 = nondet_int();
#line 170
  if (tmp___1 != 0) {
#line 172
    goto ldv_25640;
  } else {

  }

#line 187
  ldv_handler_precall();
#line 188
  exit_rc_map_tivo();
  ldv_final: 
#line 191
  ldv_check_final_state();
#line 194
  return;
}
}
#line 171 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-tivo.o.c.prepared"
void *ldv_kmem_cache_alloc_20(struct kmem_cache *ldv_func_arg1 , gfp_t flags ) 
{ 
  void *tmp ;

  {
#line 174
  ldv_check_alloc_flags(flags);
#line 175
  tmp = ldv_undef_ptr();
#line 175
  return (tmp);
}
}
#line 10 "/home/cluser/ldv/inst/kernel-rules/verifier/rcv.h"
__inline static void ldv_error(void) 
{ 


  {
  ERROR: ;
#line 12
  goto ERROR;
}
}
#line 25
extern int ldv_undef_int(void) ;
#line 7 "/home/cluser/ldv/inst/kernel-rules/kernel-model/ERR.inc"
bool ldv_is_err(void const   *ptr ) 
{ 


  {
#line 10
  return ((unsigned long )ptr > 2012UL);
}
}
#line 14 "/home/cluser/ldv/inst/kernel-rules/kernel-model/ERR.inc"
void *ldv_err_ptr(long error ) 
{ 


  {
#line 17
  return ((void *)(2012L - error));
}
}
#line 21 "/home/cluser/ldv/inst/kernel-rules/kernel-model/ERR.inc"
long ldv_ptr_err(void const   *ptr ) 
{ 


  {
#line 24
  return ((long )(2012UL - (unsigned long )ptr));
}
}
#line 28 "/home/cluser/ldv/inst/kernel-rules/kernel-model/ERR.inc"
bool ldv_is_err_or_null(void const   *ptr ) 
{ 
  bool tmp ;
  int tmp___0 ;

  {
#line 31
  if ((unsigned long )ptr == (unsigned long )((void const   *)0)) {
#line 31
    tmp___0 = 1;
  } else {
#line 31
    tmp = ldv_is_err(ptr);
#line 31
    if ((int )tmp) {
#line 31
      tmp___0 = 1;
    } else {
#line 31
      tmp___0 = 0;
    }
  }
#line 31
  return ((bool )tmp___0);
}
}
#line 20 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
int ldv_spin  =    0;
#line 24 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
void ldv_check_alloc_flags(gfp_t flags ) 
{ 


  {
#line 27
  if (ldv_spin != 0 && (flags & 16U) != 0U) {
#line 27
    ldv_error();
  } else {

  }
#line 31
  return;
}
}
#line 30
extern struct page *ldv_some_page(void) ;
#line 33 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
struct page *ldv_check_alloc_flags_and_return_some_page(gfp_t flags ) 
{ 
  struct page *tmp ;

  {
#line 36
  if (ldv_spin != 0 && (flags & 16U) != 0U) {
#line 36
    ldv_error();
  } else {

  }
#line 38
  tmp = ldv_some_page();
#line 38
  return (tmp);
}
}
#line 42 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
void ldv_check_alloc_nonatomic(void) 
{ 


  {
#line 45
  if (ldv_spin != 0) {
#line 45
    ldv_error();
  } else {

  }
#line 49
  return;
}
}
#line 49 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
void ldv_spin_lock(void) 
{ 


  {
#line 52
  ldv_spin = 1;
#line 53
  return;
}
}
#line 56 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
void ldv_spin_unlock(void) 
{ 


  {
#line 59
  ldv_spin = 0;
#line 60
  return;
}
}
#line 63 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3843/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
int ldv_spin_trylock(void) 
{ 
  int is_lock ;

  {
#line 68
  is_lock = ldv_undef_int();
#line 70
  if (is_lock != 0) {
#line 73
    return (0);
  } else {
#line 78
    ldv_spin = 1;
#line 80
    return (1);
  }
}
}
